module "dev_ec2_instance" {
  source = "../modules/ec2_instance"

  ami           = var.ami
  instance_type = var.instance_type
  instance_name = var.instance_name
}
