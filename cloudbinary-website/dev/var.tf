variable "ami" {
  description = "The AMI ID for the EC2 instance."
  default     = "ami-0fc5d935ebf8bc3bc"
}

variable "instance_type" {
  description = "The instance type for the EC2 instance."
  default     = "t2.micro"
}

variable "instance_name" {
  description = "The name of the EC2 instance."
  default     = "dev-instance"
}
