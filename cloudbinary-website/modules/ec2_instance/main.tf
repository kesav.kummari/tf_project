
provider "aws" {
  # assume_role {
  #   role_arn = "arn:aws:iam::420815905200:role/terraform-automation"
  # }
  profile = "default"
  region  = "us-east-1"
}

resource "aws_instance" "web" {
  ami           = var.ami
  instance_type = var.instance_type
  user_data = <<-EOF
              #!/bin/bash
              echo '<html><body><h1>Hello, Dev/QA Stage!</h1></body></html>' > /var/www/html/index.html
              systemctl start httpd
              systemctl enable httpd
              EOF

  tags = {
    Name = var.instance_name
  }
}

output "public_ip" {
  value = aws_instance.web.public_ip
}
