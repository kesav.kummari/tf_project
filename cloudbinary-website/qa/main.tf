module "qa_ec2_instance" {
  source = "../modules/ec2_instance"

  ami           = "ami-0fc5d935ebf8bc3bc"
  instance_type = "t2.micro"
  instance_name = "qa-instance"
}